angular.module('userService', []).factory('Usuario', function($http){
	return{
		logar: function(data){
			return $http({
				method: 'POST',
				url: '/autenticacao/login',
				headers: {'Content-Type': 'application/x-www-form-urlencoded'},
				data: $.param(data)
			});
		},
		logout: function(){
			return $http.get('/autenticacao/logout');
		}
	}
});
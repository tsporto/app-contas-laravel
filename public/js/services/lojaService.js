angular.module('lojaService', []).factory('Loja', function($http){
	return{
		get: function(){
			return $http.get('/lojas/lista');
		},
		destroy: function(id){
			return $http.delete('/lojas/delete/'+id);
		}
	}
});

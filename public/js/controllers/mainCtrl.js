angular.module('mainCtrl', []).controller('mainController', function($scope, $http, $window, Loja){  
  
  $scope.msgErro = false;

  Loja.get().success(function(data){            
      $scope.lista = data;
  });  

  $scope.delete = function(id){
    var pergunta = $window.confirm('Deseja realmente excluir?');
    if (pergunta){        
        Loja.destroy(id).success(function(data){          
          $scope.msgErro = true;        

          Loja.get().success(function(data){            
            $scope.lista = data;
          });
        }).error(function(data){          
          $scope.message = data.message;
        });
    }
  };  
});

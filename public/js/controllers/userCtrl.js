angular.module('userCtrl', []).controller('userController', function($scope, $http, $window, Usuario){

	$scope.user 		= {};
	$scope.autenticando = false;	
	$scope.erro         = false;
	$scope.msgErro      = '';

	$scope.logar = function(){				
		if($scope.user.login == null || $scope.user.senha == null){			
			return false;
		}			

		$scope.autenticando = true;
		$scope.erro 		= false;

		Usuario.logar($scope.user).success(function(data){			
			$window.location = '/home';
		}).error(function(data){
			$scope.autenticando = false;	
			$scope.erro         = true;						
			$scope.msgErro      = data.message;
		});
	};

	$scope.logout = function(){
		Usuario.logout().success(function(){
			$window.location = '/';
		})
	};
});
<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTbHosts extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hosts', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('clientes_id')->unsigned();
			$table->foreign('clientes_id')->references('id')->on('clientes');
			$table->string('dominio',150);
			$table->string('ip', 15)->nullable();

			$table->string('hospedagem', 255);
			$table->string('hospedagem_site', 255);

			$table->string('painel_endereco', 255)->nullable();
			$table->string('painel_usuario',255)->nullable();
			$table->string('painel_senha', 50)->nullable();

			$table->string('ftp_endereco', 255)->nullable();
			$table->string('ftp_usuario', 50)->nullable();
			$table->string('ftp_senha', 50)->nullable();
			$table->integer('ftp_porta')->nullable();

			$table->string('db_servidor',50)->nullable();
			$table->string('db_endereco', 255)->nullable();
			$table->string('db_usuario', 50)->nullable();
			$table->string('db_senha', 50)->nullable();
			$table->string('db_name', 50)->nullable();

			$table->string('email_smtp', 50)->nullable();
			$table->string('email_usuario', 50)->nullable();
			$table->string('email_senha', 50)->nullable();
			$table->smallinteger('email_porta')->nullable();

			$table->softDeletes();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hosts');
	}

}

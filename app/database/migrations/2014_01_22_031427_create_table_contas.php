<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableContas extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('lojas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('descricao',150);
			$table->softDeletes();
			$table->timestamps();
		});

		Schema::create('contas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('lojas_id')->unsigned();
			$table->foreign('lojas_id')->references('id')->on('lojas');
			$table->string('descricao', 255);
			$table->integer('parcela');
			$table->integer('total_parcelas');
			$table->decimal('vlr_parcela',18,2);
			$table->decimal('vlr_total',18,2);
			$table->dateTime('dt_compra');
			$table->dateTime('dt_vencimento');
			$table->dateTime('dt_baixa')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('contas');
		Schema::drop('lojas');
	}

}

<?php

Route::any('/', [
	'uses' => 'AutenticacaoController@getLogin'
]);

Route::controller('autenticacao', 'AutenticacaoController');

Route::group(array('before' => 'auth'), function(){
	Route::controller('home', 'HomeController');
	Route::controller('lojas', 'LojaController');
	Route::controller('contas', 'ContaController');
	Route::controller('user', 'UserController');
	Route::controller('site', 'SiteController');
	Route::controller('clientes', 'ClienteController');
	Route::controller('host', 'HostController');
});

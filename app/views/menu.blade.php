<ul class="navigation">
	<li>
    	<a href="/home/index"><i class="fa fa-home"></i> Home </a>    	    	
    </li>   
    <li>
        <a href="#"><i class="fa fa-group"></i> Clientes </a>
        <ul>
            <li><a href="/clientes/create">Cadastrar</a></li>
            <li><a href="/clientes">Visualizar</a></li>                            
        </ul>
    </li>                 
    <li>
    	<a href="#"><i class="glyphicon glyphicon-usd"></i> Contas </a>
    	<ul>
            <li><a href="/contas/create">Cadastrar</a></li>
        	<li><a href="/contas">Visualizar</a></li>                            
        </ul>
    </li>
    <li>
    	<a href="#"><i class="glyphicon glyphicon-tower"></i> Lojas </a>
    	<ul>
            <li><a href="/lojas/create">Cadastrar</a></li>
        	<li><a href="/lojas">Visualizar</a></li>                            
        </ul>
    </li>            
    <li>
        <a href="#"><i class="fa fa-key"></i> Usuários </a>
        <ul>
            <li><a href="/user/create">Cadastrar</a></li>
            <li><a href="/user">Visualizar</a></li>                            
        </ul>
    </li>            
    <li>
        <a href="#"><i class="fa fa-desktop"></i> Hosts (Clientes) </a>
        <ul>
            <li><a href="/host/create">Cadastrar</a></li>
            <li><a href="/host">Visualizar</a></li>                            
        </ul>
    </li>            
    <li>
        <a href="#"><i class="fa fa-lock"></i> Contas (Sites) </a>
        <ul>
            <li><a href="/site/create">Cadastrar</a></li>
            <li><a href="/site">Visualizar</a></li>                            
        </ul>
    </li>            
</ul>

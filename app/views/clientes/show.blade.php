@extends('layout')
@section('content')

	<h1> Cliente </h1>
	<p>Informações do registro</p>

	<div class="jumbotron text-center">
		<h2>{{ $value->nome }}</h2>
		<p>
			<strong>E-mail:</strong> {{ $value->email }} <br>
			<strong>Site:</strong> <a href="{{ $value->site }}" target="_blank">{{ $value->site }} </a><br>
			<strong>Contato:</strong> {{ $value->contato }} <br>
			<strong>Fone Comercial:</strong> {{ $value->fone_comercial }} <br>
			<strong>Fone Celular:</strong> {{ $value->fone_celular }} <br>
			<strong>Criado em:</strong> {{ $value->created_at }}<br>
			<strong>Última alteração:</strong> {{ $value->updated_at }}
		</p>
	</div>

	<a href="/clientes" class="btn btn-warning">Voltar</a>	
@stop
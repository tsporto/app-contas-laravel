@extends('layout')
@section('content')

	<h2><strong>Clientes</strong></h2>
    <p>Formulário de alteração</p>
    <hr>

    {{ HTML::ul($errors->all(), array('class' => 'alert alert-danger', 'style' => 'margin-bottom:30px;')) }}

    {{Form::open()}}
		{{Form::hidden('id', $value->id)}}
		<div class="form-group">		
			{{Form::label('Nome')}}						
			{{Form::text('nome', $value->nome, array('class' => 'form-control', 'autofocus'))}}			
		</div>

		<div class="form-group">		
			{{Form::label('E-mail')}}						
			{{Form::email('email', $value->email, array('class' => 'form-control'))}}			
		</div>

		<div class="form-group">		
			{{Form::label('Site')}}						
			{{Form::text('site', $value->site, array('class' => 'form-control'))}}			
		</div>

		<div class="form-group">		
			{{Form::label('Contato')}}						
			{{Form::text('contato', $value->contato, array('class' => 'form-control'))}}			
		</div>

		<div class="form-group">		
			{{Form::label('Fone Comercial')}}						
			{{Form::text('fone_comercial', $value->fone_comercial, array('class' => 'form-control'))}}			
		</div>

		<div class="form-group">		
			{{Form::label('Fone Celualr')}}						
			{{Form::text('fone_celular', $value->fone_celular, array('class' => 'form-control'))}}			
		</div>

		{{Form::submit('Salvar', array('class' => 'btn btn-success'))}}
		{{Form::reset('Cancelar', array('class' => 'btn btn-danger'))}}
	
    {{Form::close()}}

	<br>
	<a href="/clientes">Voltar</a>
@stop

@section('script')	
	<script type="text/javascript">
		$(function(){
			$('input[name=fone_comercial], input[name=fone_celular]').mask("(99) 9999-9999");

			$("form").validate({
				rules: {
					nome: {
						required: true, 
						minlength: 1,
						maxlength: 255
					},
					contato: {
						required: true, 
						minlength: 1,
						maxlength: 50
					},
					email:{
						email: true
					},
					site:{
						url: true
					},
					fone_comercial:{
						minlength: 14
					},
					fone_celular:{
						minlength: 14
					}
				},
				messages:{
					nome:{
						required: "Campo Nome é obrigatório.",
		                minlength: "Campo Nome deve conter no mínimo 1 caracteres.",
		                maxlength: "Campo Nome deve conter no máximo 255 caracteres."
					},
					contato:{
						required: "Campo Contato é obrigatório.",
		                minlength: "Campo Contato deve conter no mínimo 1 caracteres.",
		                maxlength: "Campo Contato deve conter no máximo 50 caracteres."
					},
					email:{
						email: "E-mail inválido."
					},
					site: {
						url: "Site inválido."
					},
					fone_comercial:{
						minlength: "Campo Fone deve conter 14 caracteres."
					},
					fone_celular:{
						minlength: "Campo Fone deve conter 14 caracteres."
					} 					
				}
			});
		});
	</script>
@stop
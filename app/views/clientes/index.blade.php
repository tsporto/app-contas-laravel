@extends('layout')
@section('content')

	<h2><strong>Clientes</strong></h2>
    <p>Listagem de clientes</p>
    {{HTML::linkAction('ClienteController@getCreate', 'Cadastrar', '', array('class' => 'btn btn-success'))}}    
    <br>
    <br>
	@if (Session::has('success'))
		<div class="alert alert-info">{{ Session::get('success') }}</div>
        <br>
	@endif

    <div class="block-head">
        <h2>5 latest transactions</h2>
        <div class="block-head-form">
            <button class="btn btn-info"><i class="fa fa-eye"></i> View All</button>
        </div>
    </div>
	<table class="table table-striped">
    	<thead>
    		<tr>
        		<th>ID</th>
    	       	<th>Nome</th>
    			<th>E-mail</th>    			
    			<th>Site</th>    			
    			<th>Contato</th>    			
    			<th>Fone Comercial</th>    			
                <th>Fone Celular</th>
    			<th>Ações</th>
    		</tr>
    	</thead>
    	<tbody>
    	@foreach ($clientes as $value)
    	    <tr>
			    <td>{{ $value->id }}</td>
			    <td>{{ $value->nome }}</td>
			    <td>{{ $value->email }}</td>			    
			    <td><a href="{{ $value->site }}" target="_blank">{{ $value->site }}</a> </td>				
			    <td>{{ $value->contato }}</td>
				<td>{{ $value->fone_comercial }}</td>
                <td>{{ $value->fone_celular }}</td>
				<td>       
                    <a href="/clientes/show/{{ $value->id }}" class="fa fa-check btn btn-success"></a>
                    <a href="/clientes/update/{{ $value->id }}" class="btn btn-primary fa fa-pencil"></a>
                    <a href="javascript: Enviar({{$value->id}});" class="fa fa-trash-o btn btn-danger"></a>
                </td>                
    		</tr>
    	@endforeach    
    	</tbody>
   	</table>
    
@stop

@section('script')
    <script type="text/javascript">
        function Enviar(codigo){
            $.ajax({
                url: 'clientes/delete/'+codigo,
                dataType: 'json',
                type: 'POST',
                statusCode: {
                    200: function(data){
                        alert(data);
                        window.location.reload();
                    },
                    400: function(){
                        alert('Erro ao excluir registro!');
                    }
                }
            });
        }         
    </script>
@stop
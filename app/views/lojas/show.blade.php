@extends('layout')
@section('content')

	<h1> Loja </h1>
	<p>Informações do registro</p>

	<div class="jumbotron text-center">
		<h2>{{ $loja->descricao }}</h2>
		<p>
			<strong>Criado em:</strong> {{ $loja->created_at }}<br>
			<strong>Última alteração:</strong> {{ $loja->updated_at }}
		</p>
	</div>

	<a href="/lojas" class="btn btn-info">Voltar</a>	
@stop
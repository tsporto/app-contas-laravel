@extends('layout')
@section('content')

	<h2><strong>Lojas</strong></h2>
    <p>Listagem de lojas</p>

	@if (Session::has('message'))
		<div>
			<p class="alert alert-info">{{ Session::get('message') }}</p>
			<br>
			<br>
			<br>
			<br>
		</div>
	@endif	

	<div ng-app="lojaApp" ng-controller="mainController">
		<h3 ng-show="lista.length == 0">Nenhum registro encontrado!</h3>		

		<div ng-show="msgErro">
			<p class="alert alert-success">Registro excluído com sucesso!</p>
			<br>
			<br>
			<br>
			<br>
		</div>		

		<table class="table table-striped" ng-show="lista.length > 0">
			<thead>
				<tr>
					<th>ID:</th>
					<th>Nome:</th>
					<th>Criado em:</th>
					<th>Ações:</th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="value in lista">
					<td>@{{ value.id }}</td>
					<td>@{{ value.descricao }}</td>
					<td>@{{ value.created_at }}</td>
					<td>
						<a href="/lojas/show/@{{value.id}}" class="fa fa-check btn btn-success"></a>
						<a href="/lojas/update/@{{value.id}}" class="btn btn-primary fa fa-pencil"></a>					
						<a href="" ng-click="delete(value.id)" class="fa fa-trash-o btn btn-danger">
					</td>
				</tr>
			</tbody>
		</table>
	</div>
@stop

@section('script')
	{{HTML::script('js/controllers/mainCtrl.js')}}
	{{HTML::script('js/services/lojaService.js')}}				
@stop

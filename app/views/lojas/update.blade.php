@extends('layout')
@section('content')
	
	<h2><strong>Atualizar Loja</strong></h2>
    <p>Formulário de cadastro</p>
    <hr>

    {{ HTML::ul($errors->all(), array('class' => 'alert alert-danger', 'style' => 'margin-bottom:30px;')) }}    
		
	{{Form::open()}}	
		{{ Form::hidden('id',$loja->id)}}

		<div class="form-group">
			{{Form::label('Descrição')}}
			{{Form::text('descricao', $loja->descricao, array('class' => 'form-control'))}}
		</div>    			
				
		{{Form::submit('Atualizar', array('class' => 'btn btn-success'))}}
		{{Form::reset('Cancelar', array('class' => 'btn btn-danger'))}}
    {{Form::close()}}    

    <br>
   	<a href="/lojas">Voltar</a>
@stop

@section('script')	
	<script type="text/javascript">	
		$(function(){			
			$("form").validate({
				rules: {
					descricao: {
						required: true, 
						minlength: 2,
						maxlength: 150
					}
				},
				messages:{
					descricao:{
						required: "Campo descrição é obrigatório.",
		                minlength: "Campo descrição deve conter no mínimo 2 caracteres.",
		                maxlength: "Campo descrição deve conter no máximo 150 caracteres."
					}
				}
			});
		});
	</script>
@stop




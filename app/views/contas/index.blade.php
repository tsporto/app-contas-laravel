@extends('layout')
@section('content')

	<h2><strong>Contas a pagar</strong></h2>
    <p>Listagem de contas</p>

	@if (Session::has('message'))
        <div>
            <p class="alert alert-success">{{ Session::get('message') }}</p>
            <br>
        </div>        
    @endif

    <div class="block-head">
        <h2>{{count($contas)}} registro(s) encontrado(s)</h2>
        <div class="block-head-form">
            {{HTML::linkAction('ContaController@getCreate', 'Cadastrar', '', array('class' => 'btn btn-success'))}}         
        </div>
    </div>

    @if(count($contas) > 0)
        <table class="table table-striped">
        	<thead>
        		<tr>
            		<th>ID</th>
        	       	<th>Descrição</th>
        			<th>Loja</th>
        			<th>Vencimento</th>
        			<th>Ações</th>
        		</tr>
        	</thead>
        	<tbody>
        	@foreach ($contas as $value)
        	    <tr>
        		    <td>{{ $value->id }}</td>
        		    <td>{{ $value->descricao }}</td>
        			<td>{{ isset($value->lojas->descricao) ? $value->lojas->descricao : 'Loja excluída'}}</td>
        			<td>{{ $value->dt_vencimento }}</td>
        			<td>       
                        <a href="/contas/show/{{ $value->id }}" class="fa fa-check btn btn-success"></a>             
                        <a href="/contas/update/{{ $value->id }}" class="btn btn-primary fa fa-pencil"></a>          
                        <a href="javascript: Enviar({{$value->id}});" class="fa fa-trash-o btn btn-danger"></a>
                        <a href="javascript: Baixar({{$value->id}});" class="fa fa-dollar btn btn-warning"></a>
                    </td>                
        		</tr>
        	@endforeach    
        	</tbody>
        </table>
    @endif
    
@stop

@section('script')
    <script type="text/javascript">
        function Enviar(codigo){
            $.ajax({
                url: 'contas/delete/'+codigo,
                dataType: 'json',
                type: 'POST'                
            }).done(function(data){
                alert(data);
                window.location.reload();
            });
        }  

        function Baixar(codigo){
            var pergunta = confirm('Deseja baixar o documento?');
            if (pergunta){                
                $.ajax({
                    url: 'contas/baixar/'+codigo,
                    dataType: 'json',
                    type: 'POST'                    
                }).done(function(data){
                    alert(data);
                    window.location.reload();
                });
            }
        }
    </script>
@stop
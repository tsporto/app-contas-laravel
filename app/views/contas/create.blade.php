@extends('layout')
@section('content')	

    <h2><strong>Contas a pagar</strong></h2>
    <p>Formulário de cadastro</p>
    <hr>

	{{ HTML::ul($errors->all(), array('class' => 'alert alert-danger', 'style' => 'margin-bottom:30px;')) }}

	{{Form::open()}}	
		<div class="form-group">
			{{Form::label('Descrição')}}
			{{Form::text('descricao', '', array('class' => 'form-control'))}}
    	</div>

    	<div class="form-group">
			{{Form::label('Loja')}}
			{{Form::select('lojas_id', $lojas, '', array('class' => 'form-control'))}}
    	</div>

		<div class="form-group">
			{{Form::label('Quantidade de parcelas')}}
			{{Form::text('total_parcelas', '', array('class' => 'form-control'))}}
		</div>

		<div class="form-group">
			{{Form::label('Valor da compra')}}
			{{Form::text('vlr_total', '',  array('class' => 'form-control'))}}
		</div>

		<div class="form-group">
			{{Form::label('Data da compra')}}
			{{Form::text('dt_compra', '', array('class' => 'form-control'))}}
		</div>    			
				
		{{Form::submit('Salvar', array('class' => 'btn btn-success'))}}
		{{Form::reset('Cancelar', array('class' => 'btn btn-danger'))}}
   	{{Form::close()}} 

   	<br>
   	<a href="/contas">Voltar</a>                            
@stop

@section('script')
	{{HTML::script('js/jquery.mask.min.js')}}
	
	<script type="text/javascript">
	
		$(function(){
			$("input[name=vlr_total]").mask("#.##0,00", {reverse: true, maxlength: false});
			$('input[name=dt_compra]').mask('00/00/0000');
			$('input[name=total_parcelas]').mask('0000');

			$("form").validate({
				rules: {
					descricao: {
						required: true, 
						minlength: 10
					},
					total_parcelas:{
						required: true						
					},
					vlr_total: {
						required: true						
					},
					dt_compra: {
						required: true						
					}
				},
				messages:{
					descricao:{
						required: "Campo descrição é obrigatório.",
		                minlength: "Campo descrição deve conter no mínimo 10 caracteres."
					},
					total_parcelas:{
						required: "Campo quantidade de parcelas é obrigatório."
					},
					vlr_total:{
						required: "Campo valor da compra é obrigatório."
					},
					dt_compra:{
						required: "Campo data da compra é obrigatório."						
					}
				}
			});
		});

	</script>
@stop

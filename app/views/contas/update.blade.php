@extends('layout')
@section('content')

	<h2><strong>Atualizar Contas a pagar</strong></h2>
    <p>Formulário de cadastro</p>
    <hr>
		
	{{ HTML::ul($errors->all(), array('class' => 'alert alert-danger', 'style' => 'margin-bottom:30px;')) }}

	{{Form::open()}}		
		{{Form::hidden('id', isset($conta->id) ? $conta->id : '')}}
		{{Form::hidden('dt_compra', $conta->dt_compra)}}
		{{Form::hidden('vlr_total', $conta->vlr_total)}}
		{{Form::hidden('total_parcelas', $conta->total_parcelas)}}
		{{Form::hidden('dt_vencimento', $conta->dt_vencimento)}}

		<div class="form-group">
			{{Form::label('Descrição')}}
			{{Form::text('descricao', $conta->descricao, array('class' => 'form-control'))}}
		</div>

		<div class="form-group">
			{{Form::label('Loja')}}
			{{Form::select('lojas_id', $lojas, '', isset($conta->lojas_id) ? $conta->lojas_id : Input::old('lojas_id'), array('class' => 'form-control'))}}
		</div>

 		<div class="form-group">
			{{Form::label('Parcela')}}
			{{Form::text('parcela', $conta->parcela, array('class' => 'form-control'))}}
    	</div>

		<div class="form-group">
			{{Form::label('Valor parcela')}}
			{{Form::text('vlr_parcela', $conta->vlr_parcela,  array('class' => 'form-control'))}}
		</div>
				
		{{Form::submit('Atualizar', array('class' => 'btn btn-success'))}}
		{{Form::reset('Cancelar', array('class' => 'btn btn-danger'))}}

    {{Form::close()}}    

    <br>
   	<a href="/contas">Voltar</a>
@stop

@section('script')
	{{HTML::script('js/jquery.mask.min.js')}}
	
	<script type="text/javascript">
	
		$(function(){
			$("input[name=vlr_parcela]").mask("#.##0,00", {reverse: true, maxlength: false});			
			$('input[name=parcela]').mask('0000');

			$("form").validate({
				rules: {
					descricao: {
						required: true, 
						minlength: 10
					},
					parcela:{
						required: true						
					},
					vlr_parcela: {
						required: true						
					}
				},
				messages:{
					descricao:{
						required: "Campo descrição é obrigatório.",
		                minlength: "Campo descrição deve conter no mínimo 10 caracteres."
					},
					parcela:{
						required: "Campo quantidade de parcelas é obrigatório."
					},
					vlr_parcela:{
						required: "Campo valor da compra é obrigatório."
					}
				}
			});
		});

	</script>
@stop

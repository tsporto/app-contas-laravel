@extends('layout')
@section('content')

	<h1> Conta a pagar </h1>
	<p>Informações do registro</p>

	<div class="jumbotron text-center">
		<h2>{{ $conta->lojas->descricao }}</h2>
		<p>
			<strong>Descrição:</strong> {{ $conta->descricao }}<br>
			<strong>Valor:</strong> {{ $conta->vlr_parcela }}<br>
			<strong>Valor da Compra:</strong> {{ $conta->vlr_total }}<br>
			<strong>Comprado em:</strong> {{ $conta->dt_compra }}<br>
			<strong>Vencimento:</strong> {{ $conta->dt_vencimento }}<br>
			<strong>Parcela:</strong> {{ $conta->parcela }}<br>
			<strong>Total de parcelas:</strong> {{ $conta->total_parcelas }}<br>
			<strong>Criado em:</strong> {{ $conta->created_at }}<br>
			<strong>Última alteração:</strong> {{ $conta->updated_at }}
		</p>
	</div>
	
	<a href="/contas" class="btn btn-info">Voltar</a>	
@stop
@extends('layout')
@section('content')

	<h2><strong>Usuários</strong></h2>
    <p>Listagem de usuários</p>    
	
	@if (Session::has('message'))
		<div class="alert alert-info">{{ Session::get('message') }}</div>
		<br>
	@endif

	@if(count($user) > 0)
		<table class="table table-striped">
			<thead>
				<tr>
					<th>ID:</th>
					<th>Nome:</th>
					<th>E-mail:</th>
					<th>Criado em:</th>				
					<th>Ações:</th>
				</tr>
			</thead>
			<tbody>
			@foreach($user as $value)
				<tr>
					<td>{{ $value->id }}</td>
					<td>{{ $value->nome }}</td>
					<td>{{ $value->email }}</td>
					<td>{{ $value->created_at }}</td>				
					<td>		
						<a href="/user/show/{{ $value->id }}" class="fa fa-check btn btn-success"></a>
						<a href="/user/update/{{ $value->id }}" class="btn btn-primary fa fa-pencil"></a>
						<a href="javascript: Enviar({{ $value->id }});" class="fa fa-trash-o btn btn-danger"></a>
					</td>									
				</tr>
			@endforeach
			</tbody>
		</table>			
	@else
		<br>
		<p>Nenhum registro encontrado!</p>
	@endif
@stop

@section('script')
	<script type="text/javascript">
		function Enviar(codigo){
			$.ajax({
				url: 'user/delete/'+codigo,
				dataType: 'json',
				type: 'POST'				
			}).done(function(data){
				alert(data);
				window.location.reload();
			});
		}			
	</script>
@stop
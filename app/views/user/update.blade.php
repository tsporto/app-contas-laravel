@extends('layout')
@section('content')
	
	<h2><strong>Atualizar Usuário</strong></h2>
    <p>Formulário de cadastro</p>
    <hr>

    {{ HTML::ul($errors->all(), array('class' => 'alert alert-danger', 'style' => 'margin-bottom:30px;')) }}    

	{{Form::open()}}	
		{{Form::hidden('id', $user->id)}}

		<div class="form-group">
			{{Form::label('Nome')}}
			{{Form::text('nome', $user->nome, array('class' => 'form-control'))}}
		</div>    			

		<div class="form-group">
			{{Form::label('E-mail')}}
			{{Form::email('email', $user->email, array('class' => 'form-control'))}}
		</div>    			
				
		{{Form::submit('Salvar', array('class' => 'btn btn-success'))}}
		{{Form::reset('Cancelar', array('class' => 'btn btn-danger'))}}
    {{Form::close()}}   

    <br>
   	<a href="/user">Voltar</a>                             
@stop

@section('script')	
	<script type="text/javascript">	
		$(function(){			
			$("form").validate({
				rules: {
					nome: {
						required: true, 
						minlength: 10,
						maxlength: 255
					},
					email:{
						email: true
					}
				},
				messages:{
					nome:{
						required: "Campo Nome é obrigatório.",
		                minlength: "Campo Nome deve conter no mínimo 10 caracteres.",
		                maxlength: "Campo Nome deve conter no máximo 255 caracteres."
					},
					email:{
						email: "E-mail inválido."
					}
				}
			});
		});
	</script>
@stop
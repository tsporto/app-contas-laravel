@extends('layout')
@section('content')

	<h1> Usuário </h1>
	<p>Informações do registro</p>

	<div class="jumbotron text-center">
		<h2>{{ $user->nome }}</h2>
		<p>
			<strong>E-mail:</strong> {{ $user->email }}<br>
			<strong>Login:</strong> {{ $user->login }}<br>
			<strong>Criado em:</strong> {{ $user->created_at }}<br>
			<strong>Última alteração:</strong> {{ $user->updated_at }}
		</p>
	</div>

	<a href="/user" class="btn btn-warning">Voltar</a>	
@stop
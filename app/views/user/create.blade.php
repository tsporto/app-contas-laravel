@extends('layout')
@section('content')
	
	<h2><strong>Usuários</strong></h2>
    <p>Formulário de cadastro</p>
    <hr>

    {{ HTML::ul($errors->all(), array('class' => 'alert alert-danger', 'style' => 'margin-bottom:30px;')) }}    
	{{Form::open()}}	
		<div class="form-group">
			{{Form::label('Nome')}}
			{{Form::text('nome', '', array('class' => 'form-control'))}}
		</div>    			

		<div class="form-group">
			{{Form::label('E-mail')}}
			{{Form::email('email', '', array('class' => 'form-control'))}}
		</div>    			

		<div class="form-group">
			{{Form::label('Login')}}
			{{Form::text('login', '', array('class' => 'form-control'))}}
		</div>    			

		<div class="form-group">
			{{Form::label('senha')}}
			<input type="password" name="password" class="form-control" >			
		</div>    			

		<div class="form-group">
			{{Form::label('confirmar senha')}}
			<input type="password" name="password_confirmation" class="form-control" >			
		</div>    			
				
		{{Form::submit('Salvar', array('class' => 'btn btn-success'))}}
		{{Form::reset('Cancelar', array('class' => 'btn btn-danger'))}}
    {{Form::close()}}    

    <br>
   	<a href="/user">Voltar</a>                            
@stop

@section('script')	
	<script type="text/javascript">	
		$(function(){			
			$("form").validate({
				rules: {
					nome: {
						required: true, 
						minlength: 10,
						maxlength: 255
					},
					email:{
						email: true
					},
					login: {
						required: true, 
						minlength: 6,
						maxlength: 40
					},
					password: {
						required: true, 
						minlength: 6,
						maxlength: 50
					},
					password_confirmation:{
						required: true,
						minlength: 6,
						maxlength: 50,
						equalTo: "input[name=password]"
					}
				},
				messages:{
					nome:{
						required: "Campo Nome é obrigatório.",
		                minlength: "Campo Nome deve conter no mínimo 10 caracteres.",
		                maxlength: "Campo Nome deve conter no máximo 255 caracteres."
					},
					login:{
						required: "Campo Login é obrigatório.",
		                minlength: "Campo Login deve conter no mínimo 6 caracteres.",
		                maxlength: "Campo Login deve conter no máximo 40 caracteres."
					},
					password:{
						required: "Campo Senha é obrigatório.",
		                minlength: "Campo Senha deve conter no mínimo 6 caracteres.",
		                maxlength: "Campo Senha deve conter no máximo 50 caracteres."
					},
					password_confirmation:{
						required: "Confirme sua senha.",
		                minlength: "Campo Confirmar Senha deve conter no mínimo 6 caracteres.",
		                maxlength: "Campo Confirmar Senha deve conter no máximo 50 caracteres.",
		                equalTo: "Senhas não conferem."
					},
					email:{
						email: "E-mail inválido."
					}
				}
			});
		});
	</script>
@stop
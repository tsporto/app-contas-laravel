<!-- Menu do usuário -->

<div class="profile">                    
    <img src="/tf/img/samples/users/user-30.jpg"/>
    <div class="profile-info">
        @if (Auth::check())
            <a href="#" class="profile-title">{{ Auth::user()->nome }}</a>
            <span class="profile-subtitle">{{ Auth::user()->email }}</span>
            <div class="profile-buttons">
                <div class="btn-group">                                
                    <a class="but dropdown-toggle" data-toggle="dropdown"><i class="fa fa-cog"></i></a>
                    <ul class="dropdown-menu" role="menu">                	    
                        <li><a href="/autenticacao/logout">Sair</a></li>       
                    </ul>
                </div>
            </div>            
        @endif            
    </div>
</div>

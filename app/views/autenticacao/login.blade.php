@extends('master')
@section('corpo')

<div class="page-container">            
   	<div class="page-content page-content-default">
        <div class="block-login">
   	        <div class="block-login-logo">
       	        <img src="/img/logo.png"/>
            </div>                    
            <div class="block-login-content">
 	      		<h1>Acessar o sistema</h1>

				<div ng-app="userApp" ng-controller="userController">
					
					<div style="text-align:center" ng-show="autenticando">						
						<br>
						<div class="csspinner traditional"></div>
						<br>
						<br>
						<h3>Autenticando...</h3>
					</div>

					<form ng-submit="logar()" ng-hide="autenticando">
 	      				<div class="form-group">                        								
 	      					<input type="text" class="form-control" placeholder="Usuário" autofocus name="login" ng-model="user.login">							
						</div>
						<div class="form-group">                        
							<input type="password" name="password" class="form-control" placeholder="Senha" value="" ng-model="user.senha"/>
						</div>
						<input type="submit" class="btn btn-primary btn-block">						
 	      			</form>

					<div class="sp"></div>

 	      			<div class="alert alert-danger" style="margin-bottom:20px;" ng-show="erro">        	            
                        <strong>@{{ msgErro }}</strong>
                    </div>
 	      			
				</div> 	      		                                            

                <div class="pull-left">
                    © All Rights Reserved Waib Tecnologias {{ date('Y') }}
                </div>
            </div>
        </div>
    </div>
</div>	   	    	

@stop

@section('script')	
	<script type="text/javascript">
		$(function(){
			$("form").validate({
				rules: {					
					login: {
						required: true						
					},
					password: {
						required: true						
					}
				},
				messages:{					
					login:{
						required: "Campo Usuário é obrigatório."		                
					},
					password:{
						required: "Campo Senha é obrigatório."		                
					}
				}
			});
		});
	</script>	
@stop
@extends('layout')
@section('content')

	<h1> Conta-Site </h1>
	<p>Informações do registro</p>

	<div class="jumbotron text-center">
		<h2><a href="{{ $value->site }}" target="_blank">{{ $value->site }} </a></h2>
		<p>
			<strong>Usuário:</strong> {{ $value->usuario }}<br>
			<strong>Senha:</strong> {{ Crypt::decrypt($value->senha) }}<br>
			<strong>Criado em:</strong> {{ $value->created_at }}<br>
			<strong>Última alteração:</strong> {{ $value->updated_at }}
		</p>
	</div>

	<a href="/site" class="btn btn-warning">Voltar</a>	
@stop
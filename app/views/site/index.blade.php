@extends('layout')
@section('content')

	<h2><strong>Contas (Sites)</strong></h2>
    <p>Listagem de contas</p>

	@if (Session::has('success'))
		<div class="alert alert-info">{{ Session::get('success') }}</div>
        <br>
	@endif

	<table class="table table-striped">
    	<thead>
    		<tr>
        		<th>ID</th>
    	       	<th>Site</th>
    			<th>Usuário</th>    			
    			<th>Ações</th>
    		</tr>
    	</thead>
    	<tbody>
    	@foreach ($contas as $value)
    	    <tr>
			    <td>{{ $value->id }}</td>
			    <td><a href="{{ $value->site }}" target="_blank">{{ $value->site }}</a> </td>				
				<td>{{ $value->usuario }}</td>
				<td>       
                    <a href="/site/show/{{ $value->id }}" class="fa fa-check btn btn-success"></a>
                    <a href="/site/update/{{ $value->id }}" class="btn btn-primary fa fa-pencil"></a>
                    <a href="javascript: Enviar({{$value->id}});" class="fa fa-trash-o btn btn-danger"></a>
                </td>                
    		</tr>
    	@endforeach    
    	</tbody>
   	</table>
    
@stop

@section('script')
    <script type="text/javascript">
        function Enviar(codigo){
            $.ajax({
                url: 'site/delete/'+codigo,
                dataType: 'json',
                type: 'POST',
                statusCode: {
                    200: function(data){
                        alert(data);
                        window.location.reload();
                    },
                    400: function(){
                        alert('Erro ao excluir registro!');
                    }
                }
            });
        }         
    </script>
@stop
@extends('layout')
@section('content')

	<h2><strong>Contas (Sites)</strong></h2>
    <p>Formulário de alteração</p>
    <hr>

    {{ HTML::ul($errors->all(), array('class' => 'alert alert-danger', 'style' => 'margin-bottom:30px;')) }} 

	{{Form::open()}}
		{{Form::hidden('id',$value->id)}}
		<div class="form-group">		
			{{Form::label('Site')}}						
			{{Form::text('site', $value->site , array('class' => 'form-control'))}}
			</div>
		</div> 

		<div class="form-group">
			{{Form::label('Usuário')}}
			{{Form::text('usuario', $value->usuario, array('class' => 'form-control'))}}
		</div> 

		<div class="form-group">
			{{Form::label('Senha')}}
			<input type="password" name="senha" class="form-control">			
		</div> 		

		{{Form::submit('Atualizar', array('class' => 'btn btn-success'))}}
		{{Form::reset('Cancelar', array('class' => 'btn btn-danger'))}}

	{{Form::close()}}
	
	<br>
   	<a href="/site">Voltar</a>
@stop

@section('script')
	<script type="text/javascript">	
		$(function(){			
			$("form").validate({
				rules: {
					site: {
						required: true, 
						minlength: 10,
						maxlength: 255
					},
					usuario: {
						required: true, 
						minlength: 1,
						maxlength: 255
					},
					senha: {
						required: true, 
						minlength: 6,
						maxlength: 20
					}
				},
				messages:{
					site:{
						required: "Campo Site é obrigatório.",
		                minlength: "Campo Site deve conter no mínimo 10 caracteres.",
		                maxlength: "Campo Site deve conter no máximo 255 caracteres."
					},
					usuario:{
						required: "Campo Usuário é obrigatório.",
		                minlength: "Campo Usuário deve conter no mínimo 1 caracteres.",
		                maxlength: "Campo Usuário deve conter no máximo 255 caracteres."
					},
					senha:{
						required: "Campo Senha é obrigatório.",
		                minlength: "Campo Senha deve conter no mínimo 6 caracteres.",
		                maxlength: "Campo Senha deve conter no máximo 20 caracteres."
					}
				}
			});
		});
	</script>
@stop
@extends('layout')
@section('content')

	<h2><strong>Contas (Sites)</strong></h2>
    <p>Formulário de cadastro</p>
    <hr>

    {{ HTML::ul($errors->all(), array('class' => 'alert alert-danger', 'style' => 'margin-bottom:30px;')) }} 

	{{Form::open()}}
	
		<div class="form-group">		
			{{Form::label('Site')}}						
			{{Form::text('site', 'http://', array('class' => 'form-control', 'autofocus'))}}			
		</div> 

		<div class="form-group">
			{{Form::label('Usuário')}}
			{{Form::text('usuario', '', array('class' => 'form-control'))}}
		</div> 

		<div class="form-group">
			{{Form::label('Senha')}}
			<input type="password" name="senha" class="form-control">			
		</div> 

		{{Form::submit('Salvar', array('class' => 'btn btn-success'))}}
		{{Form::reset('Cancelar', array('class' => 'btn btn-danger'))}}

	{{Form::close()}}
	
	<br>
   	<a href="/site">Voltar</a>                            
@stop

@section('script')
	<script type="text/javascript">	
		$(function(){			
			$("form").validate({
				rules: {
					site: {
						required: true, 
						minlength: 10,
						maxlength: 255
					},
					usuario: {
						required: true, 
						minlength: 1,
						maxlength: 255
					},
					senha: {
						required: true, 
						minlength: 6,
						maxlength: 20
					}
				},
				messages:{
					site:{
						required: "Campo Site é obrigatório.",
		                minlength: "Campo Site deve conter no mínimo 10 caracteres.",
		                maxlength: "Campo Site deve conter no máximo 255 caracteres."
					},
					usuario:{
						required: "Campo Usuário é obrigatório.",
		                minlength: "Campo Usuário deve conter no mínimo 1 caracteres.",
		                maxlength: "Campo Usuário deve conter no máximo 255 caracteres."
					},
					senha:{
						required: "Campo Senha é obrigatório.",
		                minlength: "Campo Senha deve conter no mínimo 6 caracteres.",
		                maxlength: "Campo Senha deve conter no máximo 20 caracteres."
					}
				}
			});
		});
	</script>
@stop
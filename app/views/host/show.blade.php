@extends('layout')
@section('content')

	<h1> Host </h1>
	<p>Informações do registro</p>

	<div class="jumbotron text-center">
		<h2>{{ $dados->clientes->nome }}</h2>
		<p>
			<strong>Domínio:</strong> {{ $dados->dominio }}<br>
			<strong>IP:</strong> {{ $dados->ip }}<br>
			<strong>Criado em:</strong> {{ $dados->created_at }}<br>
			<strong>Última alteração:</strong> {{ $dados->updated_at }}
		</p>
		<h2>Hospedagem</h2>
		<p>
			<strong>Nome:</strong> {{ $dados->hospedagem }}<br>
			<strong>Site:</strong> {{ $dados->hospedagem_site }}<br>
		</p>
		<h2>Painel de Controle</h2>
		<p>
			<strong>Endereço:</strong> {{ $dados->painel_endereco }}<br>
			<strong>Usuário:</strong> {{ $dados->painel_usuario }}<br>
			<strong>Senha:</strong> {{ $dados->painel_senha }}<br>			
		</p>
		<h2>FTP</h2>
		<p>
			<strong>Endereço:</strong> {{ $dados->ftp_endereco }}<br>
			<strong>Usuário:</strong> {{ $dados->ftp_usuario }}<br>
			<strong>Senha:</strong> {{ $dados->ftp_senha }}<br>			
			<strong>Porta:</strong> {{ $dados->ftp_porta }}<br>			
		</p>
		<h2>Banco de dados</h2>
		<p>
			<strong>Servidor:</strong> {{ $dados->db_servidor }}<br>
			<strong>Nome:</strong> {{ $dados->db_name }}<br>		
			<strong>Endereço:</strong> {{ $dados->db_endereco }}<br>
			<strong>Usuário:</strong> {{ $dados->db_usuario }}<br>
			<strong>Senha:</strong> {{ $dados->db_senha }}<br>						
		</p>
		<h2>E-mail</h2>
		<p>
			<strong>SMTP:</strong> {{ $dados->email_smtp }}<br>
			<strong>Usuário:</strong> {{ $dados->email_usuario }}<br>		
			<strong>Senha:</strong> {{ $dados->email_senha }}<br>
			<strong>Porta:</strong> {{ $dados->email_porta }}<br>			
		</p>
	</div>

	<a href="/host" class="btn btn-info">Voltar</a>	
@stop
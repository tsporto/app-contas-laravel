@extends('layout')
@section('content')

	<h2><strong>Hosts</strong></h2>
    <p>Listagem de hospedagens</p>        
    
	@if (Session::has('message'))
		<div>
            <p class="alert alert-success">{{ Session::get('message') }}</p>
            <br>
        </div>        
	@endif

    <div class="block-head">
        <h2>{{count($dados)}} registro(s) encontrado(s)</h2>
        <div class="block-head-form">
            {{HTML::linkAction('HostController@getCreate', 'Cadastrar', '', array('class' => 'btn btn-success'))}}         
        </div>
    </div>
    @if(count($dados) > 0)        
	    <table class="table table-striped">
    	    <thead>
    		    <tr>
            	   <th>ID</th>
        	       <th>Hospedagem</th>
        		   <th>Domínio</th>    			
        		   <th>Cliente</th>    			    			
        	       <th>Ações</th>
    		    </tr>
    	    </thead>
    	    <tbody>
    	    @foreach ($dados as $value)
    	        <tr>
			         <td>{{ $value->id }}</td>
			         <td>{{ $value->hospedagem }}</td>			    
			         <td><a href="{{ $value->dominio }}" target="_blank">{{ $value->dominio }}</a> </td>			
			         <td>{{ $value->clientes->nome }}</td>
				     <td>       
                        <a href="/host/show/{{ $value->id }}" class="fa fa-check btn btn-success"></a>
                        <a href="/host/update/{{ $value->id }}" class="btn btn-primary fa fa-pencil"></a>
                        <a href="javascript: Enviar({{$value->id}});" class="fa fa-trash-o btn btn-danger"></a>
                     </td>                
    		   </tr>
    	    @endforeach    
    	    </tbody>
   	    </table>
    @endif        
    
@stop

@section('script')
    <script type="text/javascript">
        function Enviar(codigo){
            $.ajax({
                url: 'host/delete/'+codigo,
                dataType: 'json',
                type: 'POST',
                statusCode: {
                    200: function(data){
                        alert(data);
                        window.location.reload();
                    },
                    400: function(){
                        alert('Erro ao excluir registro!');
                    }
                }
            });
        }         
    </script>
@stop
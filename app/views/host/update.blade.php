@extends('layout')
@section('content')

	<h2><strong>Hosts</strong></h2>
    <p>Formulário de atualização</p>
    <hr>

    {{ HTML::ul($errors->all(), array('class' => 'alert alert-danger', 'style' => 'margin-bottom:30px;')) }}

    {{Form::open()}}
    	{{Form::hidden('id', $dados->id)}}
		<div class="form-group">		
			{{Form::label('Cliente')}}						
			{{Form::select('clientes_id', $clientes, $dados->clientes_id, array('class' => 'form-control', 'autofocus'))}}			
		</div>	
		<div class="form-group">		
			{{Form::label('Domínio')}}						
			{{Form::text('dominio', $dados->dominio, array('class' => 'form-control', 'autofocus'))}}		
		</div>

		<div class="form-group">		
			{{Form::label('IP')}}						
			{{Form::text('ip', $dados->ip, array('class' => 'form-control'))}}			
		</div>
		
		<div class="block empurrar">
            <ul class="nav nav-tabs">
            	<li class="active"><a href="#tab1" data-toggle="tab">FTP</a></li>
                <li><a href="#tab2" data-toggle="tab">Hospedagem</a></li>
                <li><a href="#tab3" data-toggle="tab">Painel administrativo</a></li>                
                <li><a href="#tab4" data-toggle="tab">Banco de dados</a></li>
                <li><a href="#tab5" data-toggle="tab">SMTP</a></li>                            
            </ul>
            <div class="block-content tab-content">
            	<div class="tab-pane active" id="tab1">
                	<div class="form-group">		
						{{Form::label('Endereço')}}						
						{{Form::text('ftp_endereco', $dados->ftp_endereco, array('class' => 'form-control'))}}			
					</div>
					<div class="form-group">		
						{{Form::label('Usuário')}}						
						{{Form::text('ftp_usuario', $dados->ftp_usuario, array('class' => 'form-control'))}}			
					</div>
					<div class="form-group">		
						{{Form::label('Senha')}}						
						{{Form::text('ftp_senha', $dados->ftp_senha, array('class' => 'form-control'))}}			
					</div>
					<div class="form-group">		
						{{Form::label('Porta')}}						
						{{Form::text('ftp_porta', $dados->ftp_porta, array('class' => 'form-control'))}}			
					</div>
                </div> 
            	<div class="tab-pane" id="tab2">
                	<div class="form-group">		
						{{Form::label('Nome')}}						
						{{Form::text('hospedagem', $dados->hospedagem, array('class' => 'form-control'))}}			
					</div>
					<div class="form-group">		
						{{Form::label('Site')}}						
						{{Form::text('hospedagem_site', $dados->hospedagem_site, array('class' => 'form-control'))}}
					</div>
                </div>
                <div class="tab-pane" id="tab3">
	                <div class="form-group">		
						{{Form::label('Endereço')}}						
						{{Form::text('painel_endereco', $dados->painel_endereco, array('class' => 'form-control'))}}	
					</div>
					<div class="form-group">		
						{{Form::label('Usuário')}}						
						{{Form::text('painel_usuario', $dados->painel_usuario, array('class' => 'form-control'))}}		
					</div>
					<div class="form-group">		
						{{Form::label('Senha')}}						
						{{Form::text('painel_senha', $dados->painel_senha, array('class' => 'form-control'))}}			
					</div>
                </div>
                <div class="tab-pane" id="tab4">
                    <div class="form-group">		
						{{Form::label('Servidor')}}						
						{{Form::text('db_servidor', $dados->db_servidor, array('class' => 'form-control'))}}			
					</div>
					<div class="form-group">		
			        	{{Form::label('Endereço')}}						
			        	{{Form::text('db_endereco', $dados->db_endereco, array('class' => 'form-control'))}}			
			        </div>
                    <div class="form-group">		
                       	{{Form::label('Usuário')}}						
                       	{{Form::text('db_usuario', $dados->db_usuario, array('class' => 'form-control'))}}			
                    </div>      
                 	<div class="form-group">		
                 	  	{{Form::label('Senha')}}						
                 	   	{{Form::text('db_senha', $dados->db_senha, array('class' => 'form-control'))}}			
                 	</div>
                 	<div class="form-group">		
                 	   	{{Form::label('Nome')}}						
                 	   	{{Form::text('db_name', $dados->db_name, array('class' => 'form-control'))}}			
			        </div>
                </div>
                <div class="tab-pane" id="tab5">
                    <div class="form-group">		
				 		{{Form::label('SMTP')}}						
						{{Form::text('email_smtp', $dados->email_smtp, array('class' => 'form-control'))}}			
					</div>				
					<div class="form-group">		
						{{Form::label('Usuário')}}						
						{{Form::text('email_usuario', $dados->email_usuario, array('class' => 'form-control'))}}		
					</div>
					<div class="form-group">		
						{{Form::label('Senha')}}						
						{{Form::text('email_senha', $dados->email_senha, array('class' => 'form-control'))}}			
					</div>
					<div class="form-group">		
						{{Form::label('Porta')}}						
						{{Form::text('email_porta', $dados->email_porta, array('class' => 'form-control'))}}			
					</div>
                </div>                        
            </div>
        </div>
			
		<br>
		{{Form::submit('Atualizar', array('class' => 'btn btn-success'))}}
		{{Form::reset('Cancelar', array('class' => 'btn btn-danger'))}}
	
    {{Form::close()}}

	<br>
	<a href="/host">Voltar</a>
@stop

@section('script')	
	<script type="text/javascript">
		$(function(){		
			$("form").validate({
				rules: {
					dominio: {
						required: true, 
						minlength: 1,
						maxlength: 150
					},
					hospedagem: {
						required: true, 
						minlength: 1,
						maxlength: 255
					},
					hospedagem_site:{
						required: true, 
						minlength: 1,
						maxlength: 255
					}
				},
				messages:{
					dominio:{
						required: "Campo Domínio é obrigatório.",
		                minlength: "Campo Domínio deve conter no mínimo 1 caracteres.",
		                maxlength: "Campo Domínio deve conter no máximo 150 caracteres."
					},
					hospedagem:{
						required: "Campo Hospedagem[Nome] é obrigatório.",
		                minlength: "Campo Hospedagem[Nome] deve conter no mínimo 1 caracteres.",
		                maxlength: "Campo Hospedagem[Nome] deve conter no máximo 255 caracteres."
					},
					hospedagem_site:{
						required: "Campo Hospedagem[Site] é obrigatório.",
		                minlength: "Campo Hospedagem[Site] deve conter no mínimo 1 caracteres.",
		                maxlength: "Campo Hospedagem[Site] deve conter no máximo 255 caracteres."
					}					
				}
			});
		});
	</script>
@stop
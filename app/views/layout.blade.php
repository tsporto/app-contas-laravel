@extends('master')
@section('corpo')
	<div class="page-container">
		<!-- topo -->
		<div class="page-head">
            <ul class="page-head-elements">
                <li class="search">
                	<input type="text" class="form-control" placeholder="Search..."/>
                </li>
            </ul>
        </div>

        <div class="page-navigation">
        	<!-- Logo -->
        	<div class="page-navigation-info">
            	<img src="/img/logo.png"/>
            </div>

            @include('page-user')
        	@include('menu')
        </div>

        <!-- Conteudo -->
        <div class="page-content">
            <div class="container">

                <!-- Your content -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="block">
                            <div class="block-content">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
	</div>
@stop

<!doctype html>
<html>
<head lang="pt-br">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Waib Titan v.1.0.0</title>
    <link href="/img/icone.ico" rel="shortcut icon" />
	{{HTML::style('tf/css/styles.css')}}
	{{HTML::style('css/csspinner.min.css')}}		
</head>
<body>
	@yield('corpo')

	{{HTML::script('tf/js/plugins/jquery/jquery.min.js');}}
	{{HTML::script('tf/js/plugins/jquery/jquery-ui.min.js');}}
	{{HTML::script('tf/js/plugins/bootstrap/bootstrap.min.js');}}
	{{HTML::script('tf/js/plugins/mcustomscrollbar/jquery.mCustomScrollbar.min.js');}}
	{{HTML::script('tf/js/plugins/jquery-validation/jquery.validate.min.js');}}
	{{HTML::script('tf/js/plugins.js');}}
	{{HTML::script('tf/js/actions.js');}}
	{{HTML::script('js/jquery.mask.min.js')}}
	{{HTML::script('js/angular.min.js')}}
	
	<!-- Angular -->	
	{{HTML::script('js/controllers/userCtrl.js')}}	
	{{HTML::script('js/services/userService.js')}}		
	{{HTML::script('js/app.js')}}
	@yield('script')
</body>
</html>

<?php

class AutenticacaoController extends BaseController
{
	public function getLogin(){
		return View::make('autenticacao.login');
	}

	public function postLogin(){
		$credencial = array(
	 		'login'    => Input::get('login'),
	 		'password' => Input::get('senha')
		);
		$rules = array(
			'login'    => 'required',
			'password' => 'required'
		);
		$messages = array(
			'login.required'    => 'Informe o usuário.',
			'password.required' => 'Informe sua senha.'
		);

		$validacao = Validator::make($credencial, $rules, $messages);

		if ($validacao->passes()){
			if (Auth::attempt($credencial)){
				return Response::json(array('success' => true, 'message' => 'Logou'), 200);			
	 		}else{	 		
	 			return Response::json(array('error' => true, 'message' => 'Usuário e/ou senha inválido'), 400);
	 		}
	 	}else{
	 		return Response::json(array('error' => true, 'message' => 'Preencha os campos corretamente.'), 400);	 		
	 	}		
	}

	public function getLogout(){
		Auth::logout();
		return Redirect::action("HomeController@getIndex");
	}
}

<?php

class LojaController extends BaseController{

	public function getIndex(){
		return View::make('lojas.index');
	}

	public function getLista(){
		return Response::json(Loja::get());
	}

	public function getCreate(){
		return View::make('lojas.create');
	}

	public function getShow($id){
		return View::make('lojas.show')->with('loja',Loja::find($id));
	}

	public function postCreate(){
		try {
			$loja = new Loja();
			$v    = $loja->validate(Input::all(),0);

			if ($v->passes()){
				$loja->descricao = Input::get('descricao');
				$loja->save();
				Session::flash('message', 'Registro cadastrado com sucesso!');
			}else{
				return Redirect::to('/lojas/create')->withErrors($v->errors())->withInput();
			}
		} catch (Exception $e) {
			return Redirect::to('/lojas/create')->withErrors($e->getMessage())->withInput();
		}

		return Redirect::action('LojaController@getIndex');
	}

	public function getUpdate($id){
		$loja = Loja::find($id);
		return View::make('lojas.update')->with('loja',$loja);
	}

	public function postUpdate($id){
		try {
			$loja = new Loja();
			$v 	 = $loja->validate(Input::all(),$id);

			if ($v->passes()){
				$loja 			 = Loja::find(Input::get('id'));
				$loja->descricao = Input::get('descricao');
				$loja->save();
				Session::flash('message', 'Registro atualizado com sucesso!');
			}else{
				return Redirect::to('/lojas/update/'.Input::get('id'))->withErrors($v->errors())->withInput();
			}
		} catch (Exception $e) {
			return Redirect::to('/lojas/update/'.Input::get('id'))->withErrors($e->getMessage())->withInput();
		}

		return Redirect::action('LojaController@getIndex');
	}

	public function deleteDelete($id){
		$loja = Loja::find($id);
		if(is_null($loja)){
			return Response::json(array('error' => true, 'message' => 'Registro não encontrado!'), 404);
		}
	 	$loja->delete();
		return Response::json(array('success' => true), 200);
	}
}

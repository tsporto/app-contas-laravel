<?php 

class ContaController extends BaseController{

	public function getIndex(){	
		$dados = Conta::whereNull('dt_baixa')->orderBy('dt_vencimento')->get();
		return View::make('contas.index')->with('contas', $dados);
	}

	public function getShow($id){
		return View::make('contas.show')->with('conta',Conta::find($id));
	}

	public function getCreate(){
		$lojas       = Loja::all();
		$dados_lojas = array();

		foreach ($lojas as $value) {
			$dados_lojas[$value->id] = $value->descricao;
		}

		return View::make('contas.create')->with('lojas', $dados_lojas);
	}

	public function postCreate(){
		try {
			$dados     = Input::all();
			$conta 	   = new Conta();
			$validacao = $conta->validate($dados);
			
			if($validacao->passes()){
				for ($i=1; $i <= intval($dados['total_parcelas']) ; $i++) { 				
					$conta 	   			   = new Conta();
					$conta->lojas_id       = $dados['lojas_id'];
					$conta->descricao 	   = $dados['descricao'];
					$conta->parcela 	   = $i;
					$conta->total_parcelas = $dados['total_parcelas'];
					$conta->vlr_total 	   = implode(".", explode(",",$dados['vlr_total']));
					$conta->vlr_parcela    = $conta->vlr_total / $dados['total_parcelas'];				
					$conta->dt_compra      = implode("-",array_reverse(explode("/",$dados['dt_compra'])));
					$conta->dt_vencimento  = date_create($conta->dt_compra)->modify($i.' month')->format('Y-m-d H:i:s');
					$conta->save();	
				}
				Session::flash('success', 'Registro cadastrado com sucesso!');
			}else{
				return Redirect::to('/contas/create')->withErrors($validacao->errors())->withInput();
			}						
		} catch (Exception $e) {
			return Redirect::to('/lojas/create')->withErrors($e->getMessage())->withInput();
		}
				
		return Redirect::action('ContaController@getIndex');	
	}

	public function getUpdate($id){
		$lojas       = Loja::all();
		$dados_lojas = array();

		foreach ($lojas as $value) {
			$dados_lojas[$value->id] = $value->descricao;
		}

		$conta = Conta::find($id);
		if (!$conta){
			return Redirect::to('contas/');
		}

		return View::make('contas.update')->with('conta', $conta)->with('lojas', $dados_lojas);
	}

	public function postUpdate($id)
	{		
		$dados     = Input::all();
		$conta     = new Conta();
		$validacao = $conta->validate($dados);		

		if ($validacao->passes()) {
			$conta              = Conta::find(Input::get('id'));
			$conta->descricao   = Input::get('descricao');
			$conta->lojas_id    = Input::get('lojas_id');
			$conta->parcela     = Input::get('parcela');
			$conta->vlr_parcela = implode(".", explode(",",Input::get('vlr_parcela')));
			$conta->save();

			Session::flash('message','Registro atualizado com sucesso!');			
		}else{
			return Redirect::to('contas/update/'.$id)->withErrors($validacao->errors())->withInput();
		}		

		return Redirect::action('ContaController@getIndex');
	}

	public function postDelete($id){
		try{
			$conta = Conta::find($id);
			$conta->delete();
			return Response::json('Registro excluído com sucesso!');	
		}catch (Exception $e){								
			return Response::json('Erro: '.$e->getMessage());			
		}		
	}

	public function postBaixar($id){
		try{
			$conta = Conta::find($id);
			$conta->dt_baixa = date('Y-m-d H:i:s');
			$conta->save();
			return Response::json('Registro baixado com sucesso!');	
		}catch (Exception $e){								
			return Response::json('Erro: '.$e->getMessage());			
		}		
	}
}
<?php 

class UserController extends BaseController
{
	public function getIndex(){
		$user = User::all();
		return View::make('user.index')->with('user',$user);
	}	

	public function getCreate(){
		return View::make('user.create');
	}

	public function getUpdate($id){
		$user = User::find($id);
		return View::make('user.update')->with('user',$user);
	}

	public function getShow($id){
		$user = User::find($id);
		return View::make('user.show')->with('user',$user);	
	}

	public function postCreate(){
		try {
			$user 	   = new User();
			$validacao = $user->validate(Input::all(),0);

			if ($validacao->passes()){
				$user->nome     = Input::get('nome');
				$user->email    = Input::get('email');
				$user->login    = Input::get('login');
				$user->password = Hash::make(Input::get('password'));
				$user->save();
				Session::flash('message', 'Registro cadastrado com sucesso!');
			}else{
				return Redirect::to('/user/create')->withErrors($validacao->errors())->withInput();
			}
		} catch (Exception $e) {
			return Redirect::to('/user/create')->withErrors($e->getMessage())->withInput();
		}
		return Redirect::action('UserController@getIndex');
	}

	public function postUpdate($id){
		try {
			$user      	    			    = User::find($id);
			$dados 						    = array();
			$dados['login']				    = $user->login;			
			$dados['password'] 				= $user->password;
			$dados['password_confirmation'] = $user->password;
			$dados['nome']  			    = Input::get('nome');
			$dados['email'] 			    = Input::get('email');

			$validacao = $user->validate($dados,$id);

			if ($validacao->passes()){
				$user->nome  = Input::get('nome');
				$user->email = Input::get('email');				
				$user->save();
				Session::flash('message', 'Registro atualizado com sucesso!');
			}else{
				return Redirect::to('/user/update/'.$id)->withErrors($validacao->errors())->withInput();
			}
		} catch (Exception $e) {
			return Redirect::to('/user/update/'.$id)->withErrors($e->getMessage())->withInput();
		}
		return Redirect::action('UserController@getIndex');
	}

	public function postDelete($id){
		try{
			$user = User::find($id);
			$user->delete();
			return Response::json('Registro excluído com sucesso!');	
		}catch (Exception $e){								
			return Response::json('Erro: '.$e->getMessage());			
		}
	}
}
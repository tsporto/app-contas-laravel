<?php 

class ClienteController extends BaseController
{
	public function getIndex(){
		$cliente = Cliente::all();
		return View::make('clientes.index')->with('clientes', $cliente);
	}

	public function getCreate(){
		return View::make('clientes.create');
	}

	public function getUpdate($id){
		$cliente = Cliente::find($id);
		return View::make('clientes.update')->with('value', $cliente);
	}

	public function getShow($id){
		$cliente = Cliente::find($id);
		return View::make('clientes.show')->with('value', $cliente);
	}

	public function postCreate(){
		try {
			$cliente   = new Cliente();
			$validacao = $cliente->validate(Input::all(),0);

			if($validacao->passes()){
				$cliente->nome    = Input::get('nome');
				$cliente->contato = Input::get('contato');

				if(Input::get('email') != ''){
					$cliente->email = Input::get('email');	
				}
				if(Input::get('site') != ''){
					$cliente->site = Input::get('site');	
				}
				if(Input::get('fone_comercial') != ''){
					$cliente->fone_comercial = Input::get('fone_comercial');	
				}
				if(Input::get('fone_celular') != ''){
					$cliente->fone_celular = Input::get('fone_celular');	
				}

				$cliente->save();
				Session::flash('message', 'Registro cadastrado com sucesso!');
			}else{
				return Redirect::to('/clientes/create')->withErrors($validacao->errors())->withInput();
			}
		} catch (Exception $e) {
			return Redirect::to('/clientes/create')->withErrors($e->getMessage())->withInput();
		}

		return Redirect::action('ClienteController@getIndex');
	}

	public function postUpdate(){
		try {
			$id 	   = Input::get('id');
			$cliente   = Cliente::find($id);
			$validacao = $cliente->validate(Input::all(),$id);

			if($validacao->passes()){
				$cliente->nome    = Input::get('nome');
				$cliente->contato = Input::get('contato');

				if(Input::get('email') != ''){
					$cliente->email = Input::get('email');	
				}else{
					$cliente->email = null;
				}
				if(Input::get('site') != ''){
					$cliente->site = Input::get('site');	
				}else{
					$cliente->site = null;
				}
				if(Input::get('fone_comercial') != ''){
					$cliente->fone_comercial = Input::get('fone_comercial');	
				}else{
					$cliente->fone_comercial = null;
				}
				if(Input::get('fone_celular') != ''){
					$cliente->fone_celular = Input::get('fone_celular');	
				}else{
					$cliente->fone_celular = null;
				}

				$cliente->save();
				Session::flash('message', 'Registro cadastrado com sucesso!');
			}else{
				return Redirect::to('/clientes/update/'.$id)->withErrors($validacao->errors())->withInput();
			}
		} catch (Exception $e) {
			return Redirect::to('/clientes/update/'.$id)->withErrors($e->getMessage())->withInput();
		}

		return Redirect::action('ClienteController@getIndex');
	}

	public function postDelete($id){
		$cliente = Cliente::find($id);
		if (is_null($cliente)){
			return Response::json('Registro não encontado.',400);
		}
		$cliente->delete();
		return Response::json('Registro excluído com sucesso!',200);
	}
}
<?php 

class SiteController extends BaseController
{
	public function getIndex(){
		$contas = Site::all();
		return View::make('site.index')->with('contas', $contas);
	}

	public function getCreate(){		
		return View::make('site.create');
	}

	public function getUpdate($id){
		$site = Site::find($id);
		return View::make('site.update')->with('value',$site);	
	}

	public function getShow($id){
		$site = Site::find($id);
		return View::make('site.show')->with('value',$site);	
	}

	public function postCreate(){
		try {
			$site 	   = new Site();
			$validacao = $site->validate(Input::all());

			if($validacao->passes()){
				$site->site    = Input::get('site');
				$site->usuario = Input::get('usuario');
				$site->senha   = Crypt::encrypt(Input::get('senha'));
				$site->save();
				Session::flash('message', 'Registro cadastrado com sucesso!');
			}else{
				return Redirect::to('/site/create')->withErrors($validacao->errors())->withInput();
			}
		} catch (Exception $e) {
			return Redirect::to('/site/create')->withErrors($e->getMessage())->withInput();				
		}

		return Redirect::action('SiteController@getIndex');
	}

	public function postUpdate(){
		try {
			$id    	   = Input::get('id');
			$site  	   = Site::find($id);			
			$validacao = $site->validate(Input::all());

			if($validacao->passes()){
				$site->site    = Input::get('site');
				$site->usuario = Input::get('usuario');		
				$site->senha   = Crypt::encrypt(Input::get('senha'));		
				$site->save();
				Session::flash('message', 'Registro atualizado com sucesso!');
			}else{
				return Redirect::to('/site/update/'.$id)->withErrors($validacao->errors())->withInput();
			}
		} catch (Exception $e) {
			return Redirect::to('/site/update/'.$id)->withErrors($e->getMessage())->withInput();				
		}

		return Redirect::action('SiteController@getIndex');
	}

	public function postDelete($id){
		$site = Site::find($id);
		if(is_null($site)){
			return Response::json('Registro não encontrado!', 400);
		}
		$site->delete();
		return Response::json('Registro excluído com sucesso!', 200);
	}
}
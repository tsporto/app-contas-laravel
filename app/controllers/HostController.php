<?php 

class HostController extends BaseController
{
	public function getIndex(){
		$dados = Host::all();
		return View::make('host.index')->with('dados', $dados);
	}

	public function getCreate(){
		return View::make('host.create')->with('clientes', Host::listaClientes());
	}

	public function getShow($id){
		$dados = Host::find($id);
		return View::make('host.show')->with('dados',$dados);
	}

	public function getUpdate($id){
		$dados = Host::find($id);
		return View::make('host.update')
			->with('dados',$dados)
			->with('clientes', Host::listaClientes());
	}

	public function postCreate(){
		try {
			$model     = new Host();
			$validacao = $model->validate(Input::all(),0);

			if($validacao->passes()){				
				$model->clientes_id     = Input::get('clientes_id');
				$model->dominio         = Input::get('dominio');
				$model->hospedagem      = Input::get('hospedagem');
				$model->hospedagem_site = Input::get('hospedagem_site');

				if (Input::get('ip') != ''){
					$model->ip = Input::get('ip');		
				}
				if (Input::get('painel_endereco') != ''){
					$model->painel_endereco = Input::get('painel_endereco');		
				}
				if (Input::get('painel_usuario') != ''){
					$model->painel_usuario = Input::get('painel_usuario');		
				}
				if (Input::get('painel_senha') != ''){
					$model->painel_senha = Input::get('painel_senha');		
				}
				if (Input::get('ftp_endereco') != ''){
					$model->ftp_endereco = Input::get('ftp_endereco');		
				}
				if (Input::get('ftp_usuario') != ''){
					$model->ftp_usuario = Input::get('ftp_usuario');		
				}
				if (Input::get('ftp_senha') != ''){
					$model->ftp_senha = Input::get('ftp_senha');		
				}
				if (Input::get('ftp_porta') != ''){
					$model->ftp_porta = Input::get('ftp_porta');		
				}
				if (Input::get('db_servidor') != ''){
					$model->db_servidor = Input::get('db_servidor');		
				}
				if (Input::get('db_endereco') != ''){
					$model->db_endereco = Input::get('db_endereco');		
				}
				if (Input::get('db_usuario') != ''){
					$model->db_usuario = Input::get('db_usuario');		
				}
				if (Input::get('db_senha') != ''){
					$model->db_senha = Input::get('db_senha');		
				}
				if (Input::get('db_name') != ''){
					$model->db_name = Input::get('db_name');		
				}
				if (Input::get('email_smtp') != ''){
					$model->email_smtp = Input::get('email_smtp');		
				}
				if (Input::get('email_usuario') != ''){
					$model->email_usuario = Input::get('email_usuario');		
				}
				if (Input::get('email_senha') != ''){
					$model->email_senha = Input::get('email_senha');		
				}
				if (Input::get('email_porta') != ''){
					$model->email_porta = Input::get('email_porta');		
				}	

				$model->save();
				Session::flash('message', 'Registro cadastrado com sucesso!');
			}else{
				return Redirect::to('/host/create')->withErrors($validacao->errors())->withInput();
			}
		} catch (Exception $e) {
			return Redirect::to('/host/create')->withErrors($e->getMessage())->withInput();
		}

		return Redirect::action('HostController@getIndex');
	}

	public function postUpdate(){
		try {
			$id        = Input::get('id');
			$model     = Host::find($id);
			$validacao = $model->validate(Input::all(),$id);

			if($validacao->passes()){				
				$model->clientes_id     = Input::get('clientes_id');
				$model->dominio         = Input::get('dominio');
				$model->hospedagem      = Input::get('hospedagem');
				$model->hospedagem_site = Input::get('hospedagem_site');

				if (Input::get('ip') != ''){
					$model->ip = Input::get('ip');		
				}else{
					$model->ip = null;
				}

				if (Input::get('painel_endereco') != ''){
					$model->painel_endereco = Input::get('painel_endereco');		
				}else{
					$model->painel_endereco = null;
				}

				if (Input::get('painel_usuario') != ''){
					$model->painel_usuario = Input::get('painel_usuario');		
				}else{
					$model->painel_usuario = null;
				}

				if (Input::get('painel_senha') != ''){
					$model->painel_senha = Input::get('painel_senha');		
				}else{
					$model->painel_senha = null;
				}

				if (Input::get('ftp_endereco') != ''){
					$model->ftp_endereco = Input::get('ftp_endereco');		
				}else{
					$model->ftp_endereco = null;
				}

				if (Input::get('ftp_usuario') != ''){
					$model->ftp_usuario = Input::get('ftp_usuario');		
				}else{
					$model->ftp_usuario = null;
				}

				if (Input::get('ftp_senha') != ''){
					$model->ftp_senha = Input::get('ftp_senha');		
				}else{
					$model->ftp_senha = null;
				}

				if (Input::get('ftp_porta') != ''){
					$model->ftp_porta = Input::get('ftp_porta');		
				}else{
					$model->ftp_porta = null;
				}

				if (Input::get('db_servidor') != ''){
					$model->db_servidor = Input::get('db_servidor');		
				}else{
					$model->db_servidor = null;
				}

				if (Input::get('db_endereco') != ''){
					$model->db_endereco = Input::get('db_endereco');		
				}else{
					$model->db_endereco = null;
				}

				if (Input::get('db_usuario') != ''){
					$model->db_usuario = Input::get('db_usuario');		
				}else{
					$model->db_usuario = null;
				}

				if (Input::get('db_senha') != ''){
					$model->db_senha = Input::get('db_senha');		
				}else{
					$model->db_senha = null;
				}

				if (Input::get('db_name') != ''){
					$model->db_name = Input::get('db_name');		
				}else{
					$model->db_name = null;
				}

				if (Input::get('email_smtp') != ''){
					$model->email_smtp = Input::get('email_smtp');		
				}else{
					$model->email_smtp = null;
				}

				if (Input::get('email_usuario') != ''){
					$model->email_usuario = Input::get('email_usuario');		
				}else{
					$model->email_usuario = null;
				}

				if (Input::get('email_senha') != ''){
					$model->email_senha = Input::get('email_senha');		
				}else{
					$model->email_senha = null;
				}

				if (Input::get('email_porta') != ''){
					$model->email_porta = Input::get('email_porta');		
				}else{
					$model->email_porta = null;
				}

				$model->save();
				Session::flash('message', 'Registro atualizado com sucesso!');
			}else{
				return Redirect::to('/host/update/'.$id)->withErrors($validacao->errors())->withInput();
			}
		} catch (Exception $e) {
			return Redirect::to('/host/update/'.$id)->withErrors($e->getMessage())->withInput();
		}

		return Redirect::action('HostController@getIndex');	
	}
}
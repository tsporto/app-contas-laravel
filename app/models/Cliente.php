<?php 

class Cliente extends Eloquent{

	protected $softDelete = true;
	protected $rules      = array();

	public function validate($input, $id){
		$this->rules = array(
			'nome'    => 'required|min:1|max:255|unique:clientes,nome,'.$id,
			'contato' => 'required|min:1|max:50' 			
		);

		$messages = array(
			'required' => 'Campo :attribute é obrigatório.',
			'min' 	   => 'Campo :attribute deve conter no mínimo :min caracteres.',
			'max'	   => 'Campo :attribute deve conter no máximo :max caracteres.',
			'unique'   => 'Cliente encontra-se cadastrado.'			
		);

		return Validator::make($input, $this->rules, $messages);
	}	

	public function hosts(){
		return $this->hasMany('Host','cliente_id');
	}
}
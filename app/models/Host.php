<?php 

class Host extends Eloquent
{
	protected $table 	  = 'hosts';
	protected $rules 	  = array();
	protected $softDelete = true;

	public function validate($input, $id){
		$this->rules = array(
			'dominio'         => 'required|min:1|max:150|unique:hosts,dominio,'.$id,
			'hospedagem' 	  => 'required|min:1|max:255',
			'hospedagem_site' => 'required|min:1|max:255',
			'painel_email'    => 'email'			
		);

		$messages = array(
			'required' => 'Campo :attribute é obrigatório.',
			'min' 	   => 'Campo :attribute deve conter no mínimo :min caracteres.',
			'max'	   => 'Campo :attribute deve conter no máximo :max caracteres.',
			'email'    => 'E-mail inválido.'			
		);

		return Validator::make($input, $this->rules, $messages);
	}

	public static function listaClientes(){
		$model = Cliente::orderBy('nome','asc')->get();
		$dados = array();
		foreach ($model as $value) {
			$dados[$value->id] = $value->nome;
		}
		return $dados;
	}

	public function clientes()
	{
		return $this->belongsTo('Cliente', 'clientes_id');
	}
}
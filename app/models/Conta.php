<?php 

class Conta extends Eloquent{
	
	protected $rules = array();

	public function validate($input){
		$this->rules = array(
			'descricao' 	 => 'required|min:5|max:255',
			'total_parcelas' => 'required',
			'vlr_total'      => 'required',
			'dt_compra'      => 'required'	
		);

		$messages = array(
			'required' => 'Campo :attribute é obrigatório.',
			'min' 	   => 'Campo :attribute deve conter no mínimo :min caracteres.',
			'max'	   => 'Campo :attribute deve conter no máximo :max caracteres.'			
		);

		return Validator::make($input, $this->rules, $messages);
	}		

	public function lojas()
	{
		return $this->belongsTo('Loja', 'lojas_id');
	}
}
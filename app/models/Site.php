<?php 

class Site extends Eloquent{

	protected $softDelete = true;
	protected $rules      = array();

	public function validate($input){
		$this->rules = array(
			'site'    => 'required|min:10|max:255',
			'usuario' => 'required|min:1|max:255',
			'senha'   => 'required|min:6|max:20'
		);

		$messages = array(
			'required' => 'Campo :attribute é obrigatório.',
			'min' 	   => 'Campo :attribute deve conter no mínimo :min caracteres.',
			'max'	   => 'Campo :attribute deve conter no máximo :max caracteres.'			
		);

		return Validator::make($input, $this->rules, $messages);
	}
}
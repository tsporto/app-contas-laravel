<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class User extends Eloquent implements UserInterface, RemindableInterface {

	protected $softDelete = true;	
	protected $table 	  = 'usuarios';
	protected $rules      = array();	
	protected $hidden 	  = array('password');

	public function validate($input, $id){
		$this->rules = array(
			'nome'     => 'required|min:10|max:255',
			'login'    => 'required|min:6|max:40|unique:usuarios,login,'.$id,
			'password' => 'required|min:6|max:255|confirmed',
			'email'    => 'email'			
		);

		$messages = array(
			'required'  => 'Campo :attribute é obrigatório.',
			'min' 	    => 'Campo :attribute deve conter no mínimo :min caracteres.',
			'max'	    => 'Campo :attribute deve conter no máximo :max caracteres.',
			'unique'    => 'Login encontra-se cadastrado, escolha outro.',
			'confirmed' => 'Senha não confere.',
			'email'     => 'E-mail inválido.' 
		);

		return Validator::make($input, $this->rules, $messages);
	}
	
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	public function getAuthPassword()
	{
		return $this->password;
	}

	public function getReminderEmail()
	{
		return $this->email;
	}
}
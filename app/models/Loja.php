<?php 

class Loja extends Eloquent{
	
	protected $softDelete = true;
	protected $rules      = array();	

  	public function validate($input,$id) {	
  		$this->rules = array(                
        	'descricao' => 'required|min:2|max:150|unique:lojas,descricao,'.$id
        );	
		$messages = array(
			'required' => 'Campo :attribute é obrigatório.',
			'min' 	   => 'Campo :attribute deve conter no mínimo :min caracteres.',
			'max'	   => 'Campo :attribute deve conter no máximo :max caracteres.',
			'unique'   => 'Registro encontra-se cadastrado.'
		);

		return Validator::make($input, $this->rules, $messages);
	}

	public function contas(){
		return $this->hasMany('Conta','lojas_id');
	}
}
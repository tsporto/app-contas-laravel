<?php

return array(	
	'driver'   => 'eloquent',	
	'model'    => 'User',	
	'table'    => 'usuarios',	
	'reminder' => array(
		'email'  => 'emails.request',
		'table'  => 'token',
		'expire' => 60,
	),

);
